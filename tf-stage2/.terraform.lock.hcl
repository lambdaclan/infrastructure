# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/external" {
  version = "2.2.2"
  hashes = [
    "h1:e7RpnZ2PbJEEPnfsg7V0FNwbfSk0/Z3FdrLsXINBmDY=",
    "zh:0b84ab0af2e28606e9c0c1289343949339221c3ab126616b831ddb5aaef5f5ca",
    "zh:10cf5c9b9524ca2e4302bf02368dc6aac29fb50aeaa6f7758cce9aa36ae87a28",
    "zh:56a016ee871c8501acb3f2ee3b51592ad7c3871a1757b098838349b17762ba6b",
    "zh:719d6ef39c50e4cffc67aa67d74d195adaf42afcf62beab132dafdb500347d39",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:7fbfc4d37435ac2f717b0316f872f558f608596b389b895fcb549f118462d327",
    "zh:8ac71408204db606ce63fe8f9aeaf1ddc7751d57d586ec421e62d440c402e955",
    "zh:a4cacdb06f114454b6ed0033add28006afa3f65a0ea7a43befe45fc82e6809fb",
    "zh:bb5ce3132b52ae32b6cc005bc9f7627b95259b9ffe556de4dad60d47d47f21f0",
    "zh:bb60d2976f125ffd232a7ccb4b3f81e7109578b23c9c6179f13a11d125dca82a",
    "zh:f9540ecd2e056d6e71b9ea5f5a5cf8f63dd5c25394b9db831083a9d4ea99b372",
    "zh:ffd998b55b8a64d4335a090b6956b4bf8855b290f7554dd38db3302de9c41809",
  ]
}

provider "registry.terraform.io/louy/uptimerobot" {
  version = "0.5.1"
  hashes = [
    "h1:Oc5L04pt+5NJ8n47K7tVHbqFmuobW/x5zaF5sPGCQwE=",
    "zh:0cab4092125cf524a2e5caf74a4e90f230734b20bbceb20e51ffb82f2fcc90fc",
    "zh:130d9d9ddb2f9070d7846066d424d785a624472a25b413382bf6e80ad4754158",
    "zh:29c1ff142ec76adb751cf1e99b2c7bd9ba5f17b97bfcce5954043db9c4a83039",
    "zh:2ade3d4b911bcceb795b009105dfd58c40d27d06c2ea7402159a76559e86dd25",
    "zh:474d83e659597ee143d09100787bb35daf05dc2a0b3fbfcb8159792143b8c598",
    "zh:62694db2f947bafb00532a5b961067eb488005061e684eb74148bcbf80805270",
    "zh:6a4e423519a331328764ba0790eb79c63882e77aa8ffdf811a319b5efaf7034e",
    "zh:7e7f7c16f54690f84fcb6d1a90848bd51fc7b2eb2cc23bfa0583e18a80157393",
    "zh:a4a63a32ac0bbe0d11fda36768c824698db4c2378cbfdf5522276fe996621864",
    "zh:af3062664c5c4ec012af48dea769318dcb8ef77cbb59f15d8b6c252fb9adda85",
    "zh:de555bac4bd86d17e7b5592ec22a6db8d1496470d3dab4fa286a86e44bdad991",
  ]
}

provider "registry.terraform.io/mrparkers/keycloak" {
  version = "4.0.1"
  hashes = [
    "h1:z6heuWAzDy7WO7cbpw2QEfdZMqbF5roM6mcQX+ec4gM=",
    "zh:136b81afb4bdf7b71bcaeefde00a8e097d20199037971a552046ac197d648875",
    "zh:1cb69126e08c58cf7b67b14ecfa3999ba952f60f5ec2918796ec57486576c202",
    "zh:1d51c878d0ca7cd3014025e2e01f6d1ee7fc73e7ccb67a1833765c3183224513",
    "zh:335727454863886d6865cae3a5131cc3cca6dbfe251729669bce5d431a9b91df",
    "zh:33af47e488fdce76101c9e25b3fa9bc3c9b07caf618e194584d356a261736c13",
    "zh:68a4583a5026a87c6ce2684c54473eb9cef5408f865d0580fe5d9875c032180c",
    "zh:68c7b96c6b553018321413d2d208cd4d0ebc83942affd565c8e51d04f18dac3b",
    "zh:7868a220f477bdc4dc66449bb020fd74fc43168b66869906d025990a67a346d9",
    "zh:7a7fbe2a8e38bba5928b57fbd1e94956d1e7f72e461145fef0ada8ce7fccb645",
    "zh:87df541fdb3569204d53fe21aca032c01dc234859085cce6a9febf0ca0129183",
    "zh:8d0eaa5031a6937dcb06d0ced7ae871328c87a3cb8cd8bceef71b08c094d7a66",
    "zh:9616d1ff5ed8377920f9b89eac0cf2103969d2cde829dac55e7a2c3e208baa97",
    "zh:99af64a38b7f5e3a7c714cb485321280a83dfed2efcbe0751923aca725fb6d51",
    "zh:b3c1977bad48f8df311a9f25e7fa2a57ff175768cc548533f4d4a2b8652e5b9a",
    "zh:c6b97dd6162934155454f3f891c4f32185af9e48ceb5e2d71dd7dce74f95efcf",
    "zh:c924c9dabaa64b8dcef39bda9b67af27a714eb87ee1e01bd404b5823dc604b18",
    "zh:db3b4d02fef69217055ac1536902bb694f3800c5d9929c7032ab31a3bd7147e1",
  ]
}

provider "registry.terraform.io/vexxhost/uptimerobot" {
  version = "0.8.2"
  hashes = [
    "h1:TiAJg4VrgNreABRH7uTFg8HOp0KY53iVw/M+MVO0D9Y=",
    "zh:348b22a27496b3f88103f6a08791d51531b0b20c35a7cf32c7dcf6a9a5b58d26",
    "zh:77cdae6f3c852c677a70be4bc335372aba7ceef9556d4b4427bbc683030d5f7d",
    "zh:7ef18e4bae4d9e92bdda3e4b5c633f1b7e614a9f8613df01d88334a4d86b1f99",
    "zh:aa46d603a3dce7651ebb84e6b2b0419010594efbc8ab2d57c9f4906590f2c43f",
  ]
}
